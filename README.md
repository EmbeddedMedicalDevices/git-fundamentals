
[![pipeline status](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/git-fundamentals/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/git-fundamentals/-/commits/main)

# Git Fundamentals

* HTML Slides: https://embeddedmedicaldevices.pages.oit.duke.edu/git-fundamentals/git-fundamentals.html
* PDF Slides: https://embeddedmedicaldevices.pages.oit.duke.edu/git-fundamentals/git-fundamentals.pdf
